# Radix
Minecraft-compatible client using libgdx.

## Project Goal
Radix was originally created so that I could learn OpenGL (although its since been moved from LWJGL to the more abstracted libgdx).

More recently the project has been transformed into an attempt to make a faster and cleaner Minecraft client.

## Features
* Join Minecraft servers
* Walk around
* Break blocks (creative, some survival support)
* Place blocks
* Render the world
* Chat

## Screenshots
![Shot 1](https://lambda.sx/G5C.png)
![Wireframe](https://lambda.sx/hLh.jpg)
